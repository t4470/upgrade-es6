// Iteración #1: Arrows

/* Crea una arrow function que tenga dos parametros a y b y 
que por defecto el valor de a = 10 y de b = 5. Haz que la función muestre 
por consola la suma de los dos parametros. */

const sumDefault = (a = 10, b = 5) => {
	return a + b;
}
// 1.1 Ejecuta esta función sin pasar ningún parametro
console.log(sumDefault()); // devuelve 15

// 1.2 Ejecuta esta función pasando un solo parametro
console.log(sumDefault(4)); // devuelte 9 (4 + 5 default)

// 1.3 Ejecuta esta función pasando dos parametros
console.log(sumDefault(4,20)); // devuelve 24 (4 + 20) los valores default han sido reemplazados
