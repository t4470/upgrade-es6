// Iteración #5: Filter

/* 5.1 Dado el siguiente array, utiliza .filter() para generar un nuevo array 
con los valores que sean mayor que 18 */
const ages = [22, 14, 24, 55, 65, 21, 12, 13, 90];

const older18 = ages.filter((age) => {
	return age > 18
});

console.log(older18);

/* 5.2 Dado el siguiente array, utiliza .filter() para generar un nuevo array 
con los valores que sean par. */
const ages2 = [22, 14, 24, 55, 65, 21, 12, 13, 90];

const ageEven = ages2.filter((age) => {
	return age % 2 == 0
})
console.log(ageEven);

/* 5.3 Dado el siguiente array, utiliza .filter() para generar un nuevo array 
con los streamers que tengan el gameMorePlayed = 'League of legends'. */
const streamers = [
	{name: 'Rubius', age: 32, gameMorePlayed: 'Minecraft'},
	{name: 'Ibai', age: 25, gameMorePlayed: 'League of Legends'}, 
	{name: 'Reven', age: 43, gameMorePlayed: 'League of Legends'},
	{name: 'AuronPlay', age: 33, gameMorePlayed: 'Among Us'}
];

const lolMorePlayed = streamers.filter(streamer => 
	streamer.gameMorePlayed === 'League of Legends');

console.log(lolMorePlayed);

/* 5.4 Dado el siguiente array, utiliza .filter() para generar un nuevo array 
con los streamers que incluyan el caracter 'u' en su propiedad .name. Recomendamos 
usar la funcion .includes() para la comprobación. */
const streamers2 = [
	{name: 'Rubius', age: 32, gameMorePlayed: 'Minecraft'},
	{name: 'Ibai', age: 25, gameMorePlayed: 'League of Legends'},
	{name: 'Reven', age: 43, gameMorePlayed: 'League of Legends'},
	{name: 'AuronPlay', age: 33, gameMorePlayed: 'Among Us'}
];

const nameU = streamers2.filter((streamer) => {
	return streamer.name.includes('u')
});
console.log(nameU)
/* 5.5 utiliza .filter() para generar un nuevo array con los streamers que incluyan 
el caracter 'Legends' en su propiedad .gameMorePlayed. Recomendamos usar la funcion 
.includes() para la comprobación.
Además, pon el valor de la propiedad .gameMorePlayed a MAYUSCULAS cuando 
.age sea mayor que 35. */
const lolMorePlay = streamers2.filter((streamer) => {
	if(streamer.age > 35) {
		streamer.gameMorePlayed = streamer.gameMorePlayed.toUpperCase()
		return streamer.gameMorePlayed
	}
	return streamer.gameMorePlayed.includes('Legends')
})
console.log(lolMorePlay)

 // (CORREGIDO CON ALVARO YO SOLO NO PUDE SACARLO)
/* 5.6 Dado el siguiente html y javascript, utiliza .filter() para mostrar por consola 
los streamers que incluyan la palabra introducida en el input. De esta forma, si 
introduzco 'Ru' me deberia de mostrar solo el streamer 'Rubius'. Si
introduzco 'i', me deberia de mostrar el streamer 'Rubius' e 'Ibai'. */

const streamers3 = [
	{name: 'Rubius', age: 32, gameMorePlayed: 'Minecraft'},
	{name: 'Ibai', age: 25, gameMorePlayed: 'League of Legends'},
	{name: 'Reven', age: 43, gameMorePlayed: 'League of Legends'},
	{name: 'AuronPlay', age: 33, gameMorePlayed: 'Among Us'}
];

const input = document.querySelector('[data-function="toFilterStreamers"]');

const searchStreamersFilter = () => {
	const input = document.querySelector('[data-function="toFilterStreamers"]');

	const findStreamers = streamers3.filter((streamer) => {
		if(streamer.name.toLowerCase().includes(input.value.toLowerCase())) {
			return streamer;
		}
	})
	console.log(findStreamers)
}

input.addEventListener('input', searchStreamersFilter);




// HECHO CON ALVARO
/* 5.7 Dado el siguiente html y javascript, utiliza .filter() para mostrar por consola 
los streamers que incluyan la palabra introducida en el input. De esta forma, si 
introduzco 'Ru' me deberia de mostrar solo el streamer 'Rubius'. Si introduzco 'i', 
me deberia de mostrar el streamer 'Rubius' e 'Ibai'.
En este caso, muestra solo los streamers filtrados cuando hagamos click en el button. */
const streamers4 = [
	{name: 'Rubius', age: 32, gameMorePlayed: 'Minecraft'},
	{name: 'Ibai', age: 25, gameMorePlayed: 'League of Legends'},
	{name: 'Reven', age: 43, gameMorePlayed: 'League of Legends'},
	{name: 'AuronPlay', age: 33, gameMorePlayed: 'Among Us'}
];

const $$button = document.querySelector('[data-function="toShowFilterStreamers"]');
const $$input = document.querySelector('[data-function="toFilterStreamers"]');

const searchStreamers = () => {
	const $$input = document.querySelector('[data-function="toFilterStreamers"]');

	const findStreamers = streamers4.filter((streamer) => {
		if(streamer.name.toLowerCase().includes($$input.value.toLowerCase())) {
			return streamer;
		}
	})
	console.log(findStreamers)
}

$$button.addEventListener('click', searchStreamers);